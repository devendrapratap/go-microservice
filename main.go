package main

import (
	"log"

	"github.com/devendrapratap/microservice/cmd"
)

func main() {
	if err := cmd.RootCommand().Execute(); err != nil {
		log.Fatal(err)
	}
}
